const csv = require('csv-parser');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

// Define the input files
const file1 = './file1.csv';
const file2 = './file2.csv';

// Define the output file
const outputFile = 'out.csv';

// Define the headers for the output file
const csvWriter = createCsvWriter({
    path: outputFile,
    header: [
        {id: 'profile_id', title: 'profile_id'},
        {id: 'email', title: 'email'},
        {id: 'name', title: 'name'},
        {id: 'created_at', title: 'created_at'},
        {id: 'total_exp', title: 'total_exp'},
        {id: 'highest_qualification', title: 'highest_qualification'},
        {id: 'company_name', title: 'company_name'},
        {id: 'skills', title: 'skills'}
    ]
});

const mergedData = {};

fs.createReadStream(file1)
    .pipe(csv())
    .on('data', (data) => {
        mergedData[data.email] = data;
    })
    .on('end', () => {
        fs.createReadStream(file2)
            .pipe(csv())
            .on('data', (data) => {
                if (mergedData[data.email]) {
                    mergedData[data.email] = {
                        ...mergedData[data.email],
                        ...data
                    };
                }
            })
            .on('end', () => {
                const records = Object.values(mergedData);
                csvWriter.writeRecords(records)
                    .then(() => {
                        console.log('Merged file written to', outputFile);
                    });
            });
    });
